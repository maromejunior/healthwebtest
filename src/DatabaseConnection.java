import java.sql.*;
import java.util.*;
import java.util.stream.IntStream;

import static java.lang.System.*;

public class DatabaseConnection
{
    // Create {@code Scanner} object reference {@code scan}
    private final Scanner scan = new Scanner(System.in);

    // Create {@code Connection} object reference variable {@code connection}
    private final Connection connectToAddress;

    // Create {@code PreparedStatement} object reference variable {@code selectStudents}
    private final PreparedStatement selectStudents;

    // Create {@code PreparedStatement} object reference variable {@code selectGrades}
    private final PreparedStatement selectGrades;

    // Create {@code PreparedStatement} object reference variable {@code insertStudent}
    private final PreparedStatement insertStudent;

    // Create {@code PreparedStatement} object reference variable {@code updateStudentById}
    private final PreparedStatement updateStudentById;

    // Create {@code PreparedStatement} object reference variable {@code deleteStudent}
    private final PreparedStatement deleteStudent;

    // Declare {@code boolean} variable {@code isConnected}.
    private boolean isConnected;

    // Declare {@code ResultSet} object reference variable {@code resultSet}
    private ResultSet resultSet;

    // Declare {@code ResultSetMeta} object reference variable {@code metaData}
    private ResultSetMetaData metaData;

    /*
     * Declare {@code ArrayList} object reference variable {@code columnName},
     * to store the column names returned by the {@code metaData} reference.
     */
    private ArrayList<String> columnName;

    /**
     * Create user-defined constructor to initialize {@code connection},
     * {@code retrieveStudents}, and {@code retrieveGrade}
     *
     * @param url      Specifies a database url of the form
     *                 <code>jdbc:<em>subprotocol</em>:<em>subname</em></code>
     *
     * @param username Specifies the database user on whose behalf the connection is being made
     *
     * @param password Specifies the database password
     *
     * @throws SQLException If a database access error occurs or the url is {@code null}
     *
     * Expected Input:  <ol>
     *                    <li> String url = "jdbc:mysql://localhost/practising" </li>
     *                    <li> String username = "root"                         </li>
     *                    <li> String password = ""                             </li>
     *                  </ol>
     *
     * Expected Output: <boolean> True </boolean>
     */
    public DatabaseConnection( String url, String username, String password ) throws Exception
    {
         Class.forName("com.mysql.jdbc.Driver");
        /*
         * Initialize {@code connection} with the result of a call to static method {@code getConnection}
         * (takes two arguments) of class {@code DriverManager}, which attempts to connect to the database
         * specified by the URL, username, and password.
         *
         */
        connectToAddress = DriverManager.getConnection( url, username, password );

        /*
         * Initialize {@code selectStudents}, {@code selectGrades}, {@code insertStudent},
         * {@code updateStudentById} and {@code deleteStudent} with the query result by
         * invoking {@code Connection method {@code prepareStatement} (takes a query argument)
         * and returns the query data.
         *
         * Note: Some drivers may not support pre-compilation. In this case,
         * the statement may not be sent to the database until the
         * <code>PreparedStatement</code> object is executed or the corresponding
         * escape characters are used for MariaDb or MySQL.
         *
         * To avoid SQL Injections during data transfer, we use
         * <code>PreparedStatement</code> with wildcard characters to prevent
         * unauthorised data modification.
         *
         */
        selectStudents = connectToAddress.prepareStatement("SELECT `STUDENT NUMBER`," +
                " `STUDENT NAME`,  `GPA` FROM student");

        selectGrades = connectToAddress.prepareStatement( "SELECT `STUDENT NAME`, `COURSE DESCRIPTION`," +
                "`MARK`, `GRADE` FROM `student` " +
                " INNER JOIN `grade`" +
                " ON `student`.`STUDENT NUMBER` = `grade`.`STUDENT NUMBER`" +
                " INNER JOIN `course`" +
                " ON `grade`.`COURSE ID` = `course`.`COURSE ID`");

        insertStudent = connectToAddress.prepareStatement("INSERT INTO `student` (`STUDENT NUMBER`, `STUDENT NAME`, " +
                "`TOTAL CREDITS`,`GPA`, `ADVISOR NUMBER`) VALUES(?, ?, ?, ?, ?)");

        updateStudentById = connectToAddress.prepareStatement("UPDATE `grade` SET `GRADE` = ?, `MARK` = ? " +
                "WHERE `STUDENT NUMBER` = ?" );

        deleteStudent = connectToAddress.prepareStatement("DELETE FROM `student` WHERE `student`.`STUDENT NUMBER` = ?");

        /*
         * This statement will only be executed if the database connection
         * is successful; if an error occurs while compiling the above statements,
         * program execution is thrown back to the {@code SQLException}. If successful,
         * reassign {@code isConnected} variable to true.
         */
        isConnected = true;
    } // end DatabaseConnection constructor

    /**
     * Properly aligns the column labels and displays the generated table.
     *
     * @param start_width The {@code String} format specifier for aligning
     *                    the first column spacing value.
     *
     * @param end_width The {@code String} format specifier for aligning
     *                  the last column width.
     */
    public void displayColumnName( String start_width, String end_width )
    {
        /*
         * Declare implicit type value inferred from the
         * {@code columnName.size()} method to retrieve
         * the number of elements in the {@code ArrayList}
         */
        int size = columnName.size();

        /*
         * Use a lambda function to iterate the {@code ArrayList}
         * and invoke {@code range()} method from the {@code IntStream}
         * class and pass in the initial value and the upper bound inside
         * the parameter-list.
         */
        IntStream.range( 0, size ).forEach( index -> out.printf(( index != 2 )
                        ? start_width : String.format( "%-9s", end_width ),  columnName.get( index )));
        out.println();
    }

    /**
     * Inserts the database column names into the {@code ArrayList}
     * by iterating over the {@code ResultSetMetaData} object sequentially.
     *
     * @param metaData The {@code ResultSetMetaData} data on which to perform operations.
     */
    private void getColumnName( ResultSetMetaData metaData )
    {
        columnName = new ArrayList<>();

        assert isConnected : "Connection to database not established.";

        try
        {
            int row_count = metaData.getColumnCount();

            IntStream.rangeClosed( 1, row_count ).forEach( index ->
            {
                try
                {
                    columnName.add( metaData.getColumnName( index ));
                }
                catch ( SQLException sqlException )
                {
                    database_logger( sqlException );
                    closeConnection();
                }
            });
        }
        catch ( SQLException sqlException ) {
            database_logger( sqlException );
        }
    } // end getColumnName method

    /**
     * Retrieves the designated query results and assigns them to
     * the {code Student} class.
     * @return The student data returned by the <code>ResultSet</code>
     */
    public ArrayList< Patient > getPatients()
    {
        ArrayList< Patient > studentData = null;
        resultSet = null;
        metaData = null;

        try
        {
            resultSet = selectStudents.executeQuery();
            metaData = resultSet.getMetaData();
            getColumnName( metaData );

            studentData = new ArrayList<>();

            while ( resultSet.next() )
            {
                studentData.add(new Patient(
                        resultSet.getInt("STUDENT NUMBER"),
                        resultSet.getString("STUDENT NAME"),
                        resultSet.getDouble("GPA")));
            }
        }
        catch ( SQLException sqlException )
        {
            database_logger( sqlException );
            closeConnection();
        }
        return studentData;
    } // end method getStudents

    /**
     * Retrieves the database results from this query and
     * adds them to {@code ArrayList} to allow for easy
     * manipulation and processing.
     * @return The query results stored in {@code ArrayList}
     */
    public ArrayList< Grade > getStatistics()
    {
        List< Grade > queryData = null;
        resultSet = null;

        try {
            resultSet = selectGrades.executeQuery();
            resultSet.last();

            metaData = resultSet.getMetaData();
            getColumnName( metaData );

            queryData = new ArrayList<>();

            do {
                queryData.add( new Grade(
                        resultSet.getString("STUDENT NAME"),
                        resultSet.getString("COURSE DESCRIPTION"),
                        resultSet.getInt("MARK"),
                        resultSet.getString("GRADE")));
            } while ( resultSet.previous() );
        }
        catch ( SQLException sqlException ) {
            database_logger( sqlException );
            closeConnection();
        }

        return (ArrayList< Grade >) queryData;
    } // end method getGrades

    /**
     * Deallocate resources that an object has used by manually
     * closing the {@code Connection} and {@code PreparedStatement},
     * which are considered limited resources. Before closing, the method
     * verifies if the database connection was established by checking
     * {@code isConnected} status value.
     * @author Paul Deitel
     * @author Paul Ntshabeleng
     */
    private void closeConnection()
    {
        if ( isConnected || ( connectToAddress != null ))
        {
            try
            {
                connectToAddress.close();
                selectStudents.close();
                selectGrades.close();
                insertStudent.close();
                updateStudentById.close();
                deleteStudent.close();
            }
            catch (SQLException sqlException)
            {
                database_logger(sqlException);
            }
            finally
            {
                /*
                 * Executes regardless of the try-catch clause;
                 * Reassign {@code isConnected} to false
                 */
                isConnected = false;
            }
        }
    } // end method closeConnection

    /**
     * Displays the error log messages containing a guide-line
     * about the cause of the exception.
     * @param exception The caught catch thrown by the caller.
     */
    public static void database_logger( Exception exception )
    {
        StackTraceElement[] stack_trace = exception.getStackTrace();

        out.printf("%-59s%-43s%-14s%-14s%n",
                "CLASS", "FILE", "LINE", "METHOD");

        int bound = stack_trace.length;

        int index = 0;

        while (index < bound)
        {
            out.printf("%-59s%-43s%-14s%-14s%n",
                    stack_trace[index].getClassName(),
                    stack_trace[index].getFileName(),
                    stack_trace[index].getLineNumber(),
                    stack_trace[index].getMethodName());
            index++;
        }
    } // end method database_logger

    /**
     * Executes the corresponding method specified by the user
     * input retrieved from the {@code Scanner} object.
     * @param input
     */
    public void optionalQueries( int input )
    {
        boolean shouldContinue = true;
        resultSet = null;

        switch( input )
        {
            case 1:

                while ( shouldContinue )
                {
                    try
                    {
                        out.println( "Please enter Student Number:");
                        int student_No = scan.nextInt();

                        out.println( "Please enter Student Name:");
                        scan.nextLine();
                        String studentName = scan.nextLine();

                        out.println( "Please enter Total Credits:");
                        int credits = scan.nextInt();

                        out.println( "Please enter Gpa:");
                        double gpa = scan.nextDouble();

                        out.println( "Please enter Advisor Number:");
                        int advisor_No = scan.nextInt();

                        addStudent( student_No, studentName, credits, gpa, advisor_No );
                        shouldContinue = false;
                    }
                    catch ( Exception cause )
                    {
                        database_logger( cause );
                        out.println("Invalid input. Try again\n:");
                        scan.nextLine();
                    }
                }
                break;

            case 2:

                while ( shouldContinue )
                {
                    try
                    {
                        out.println("Please enter Student Grade Symbol:");
                        String grade = scan.next();

                        out.println("Please enter Student Mark:");
                        int mark = scan.nextInt();

                        out.println("Please Student Id:");
                        int student_Id = scan.nextInt();

                        doUpdateRecord( grade, mark, student_Id );
                        shouldContinue = false;
                    }
                    catch( Exception exception )
                    {
                        database_logger( exception );
                        out.println("Invalid input. Try again\n:");
                        scan.nextLine();
                    }
                }
                break;

            case 3:
                out.println("Please Student Id:");
                int studentId;

                while ( shouldContinue )
                {
                    try
                    {
                        studentId = scan.nextInt();
                        deleteRecord( studentId );
                        shouldContinue = false;
                    }
                    catch( Exception incorrectNumberSupplied )
                    {
                        database_logger( incorrectNumberSupplied );
                        out.println("Please enter a valid Student Id:");
                        scan.nextLine();
                    }
                }
                break;
        }
    } // end method OptionalQueries

    /**
     *
     * @param student_No The specified student number to add.
     *
     * @param studentName The specified student name to add.
     * @param credits The specified credits to add.
     * @param gpa The specified gpa value to add.
     * @param advisor_No The specified advisor number to add.
     */
    private void addStudent( int student_No, String studentName, int credits, double gpa, int advisor_No )
    {
        try
        {
            insertStudent.setInt(1, student_No);
            insertStudent.setString(2, studentName);
            insertStudent.setInt(3, credits);
            insertStudent.setDouble(4, gpa);
            insertStudent.setInt(5, advisor_No);

            insertStudent.executeUpdate();
        }
        catch ( SQLException sqlException)
        {
            database_logger( sqlException );
            closeConnection();
        }
    } // end method addStudent

    /**
     *
     * @param grade The specified grade character (constant)
     * @param mark The mark specified by the user (constant)
     * @param student_Id The student id integer (constant)
     */
    private void doUpdateRecord( String grade, int mark, int student_Id )
    {
        try
        {
            /*
             *  Sets the designated parameter values {@code grade} of type
             *  <code>VARCHAR</code> a single character, {@code mark}
             *  of type <code>INT</code> a whole number, and {@code student_Id}
             *  of type <code>INT</code>. Invokes the identical methods for each
             *  type from the {@code PreparedStatement} class, which converts them
             *  to an SQL type value depending.
             */
            updateStudentById.setString(1, grade );
            updateStudentById.setInt(2, mark );
            updateStudentById.setInt(3, student_Id );

            /*
             * Invokes <code>PreparedStatement</code> {@code executeUpdate} method
             * which returns either (1) or count of the number of rows
             * that were successfully updated.
             */
            updateStudentById.executeUpdate();

            /*
             * If an error occurs in any of the above statements,
             * abort mission of {@see L2} and throw SQLException.
             */
        }
        catch ( SQLException sqlException )
        {
            database_logger( sqlException );
            closeConnection();
        }
    } // end method doUpdateRecord

    /**
     * Deletes a database record (with a clause)
     * specified by the {@code studentId} by executing
     * the <code>DELETE</code> SQL statement.
     *
     * @param studentId The student id (constant) to operate on.
     */
    private void deleteRecord( int studentId )
    {
        try
        {
            /*
             * Invokes  <code>PreparedStatement</code> {@code setInt}
             * (pass in an int and {@code studentId} to remove the
             * designated record.
             */
            deleteStudent.setInt(1, studentId);
deleteStudent.cancel();
            /*
             * Invokes <code>PreparedStatement</code> {@code executeUpdate} method
             * which returns either (1) or count of the number of rows
             * that were successfully updated.
             */
            deleteStudent.executeUpdate();
        }
        catch ( SQLException sqlException )
        {
            database_logger( sqlException );
            closeConnection();
        }
    } // end method deleteRecord
} // end class DatabaseConnection