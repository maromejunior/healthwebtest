public class Patient
{
    private static final long serialVersionUID = 1L;
    private int patient_number;
    private String patient_name;
    private double gpa;

    /* default class constructor */
    public Patient(){}

    /**
     * Define class constructor to instantiate the instance variables;
     * if the constructor uses parameter names identical to instance
     * variable names the 'this' reference is required to distinguish
     * between the names.
     *
     * @param patient_number The patient number supplied.
     * @param patient_name The patient name supplied
     * @param gpa The gpa assigned
     */
    Patient(int patient_number, String patient_name, double gpa) {
        this.patient_number = patient_number;
        this.patient_name = patient_name;
        this.gpa = gpa;
    }

    /**
     * Gets the patient number assigned to the instance variable.
     * @return The patient number
     */
    public int getPatient_number()
    {
        return patient_number;
    }

    /**
     * Gets the patient name assigned to the class variable.
     * @return The patient name
     */
    public String getPatient_name() {
        return patient_name;
    }

    /**
     * Gets the patient health rate assigned to the class variable
     * @return The patient's health rate.
     */
    public double getAverage() {
        return gpa;
    }

    /**
     * Overrides the {@code String} class {@code toString} method with
     * the predefined one.
     *
     * @return the string representation of the class object to the caller.
     */
    @Override
    public String toString() {
        return String.format("%-19s%-27s%s",
                getPatient_number(), getPatient_name(), getAverage());
    }
}