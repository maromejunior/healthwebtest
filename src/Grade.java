public class Grade {

    String studentName;
    private String courseDescription;
    private String gradeSymbol;
    private int mark;

    Grade(String studentName, String courseDescription, int mark, String gradeSymbol)
    {
        this.studentName = studentName;
        this.courseDescription = courseDescription;
        this.mark = mark;
        this.gradeSymbol = gradeSymbol;
    }

    public Grade(){
        this("", "", 0, "");
    }

    private String getStudentName() {
        return studentName;
    }

    public String getCourseDescription() { return courseDescription; }

    public int getMark() {
        return mark;
    }

    private String getGradeSymbol() {
        return gradeSymbol;
    }

    @Override
    public String toString()
    {
        return String.format("%-26s%-32s%s%8s",
                getStudentName(), getCourseDescription(), getMark(), getGradeSymbol());
    }
}