import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

import static java.lang.System.out;

public class StatisticalTest {

    final String URL = "jdbc:mysql://localhost/practising";
    final String USERNAME = "root";
    final String PASSWORD = "";
    final static String SELECT_QUERY = "SELECT `PATIENT NUMBER`, PATIENT NAME` FROM patient";

    // Create {@code DatabaseConnect} object reference variable {@code coreConnectTask}
    private static DatabaseConnection coreConnectTask;

    // Create {@code Scanner} object reference variable {@code input}
    private static Scanner input = new Scanner(System.in);

    /**
     * Main method begins program execution
     * @param args Command line arguments
     */
    public static void main(String[] args)
    {
        try {
            out.println("Go for prospects, go for the pitch!!");
            out.println("Database Request for Displaying Patient Number and Patient Name\n");
            connectDatabase();
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        contentManager();
    } // end method main

    /**
     * Aligns the query results stored in the {@code ArrayList} by invoking the
     * {@code DatabaseConnect} class method {@code getStudents} to display the student
     * results sequentially with the database column headers.
     */
    private static void students()
    {
        List<Patient> studentQuery = coreConnectTask.getPatients();
        classData(studentQuery, "%-19s", "%11s");
        out.println();
    } // end method students

    /**
     * Aligns the query results specific to the student grades; invokes
     * {@code classData} method to neatly display the database results in
     * descending order as specified by the {@code ResultSet}
     */
    private static void grades()
    {
        List<Grade> gradeQuery = coreConnectTask.getStatistics();
        classData(gradeQuery, "%-26s", "%10s");
    } // end method grades

    private static <T> void classData( List<T> typeQuery, String rowFormat, String colFormat )
    {
        coreConnectTask.displayColumnName(rowFormat, colFormat);
        forEach(typeQuery.size(), typeQuery);
    } // end method classData

    private static <T> void forEach(int length, List<T> type)
    {
        IntStream.range(0, length).forEach(index ->
                out.printf("%-19s%n", type.get(index)));
        out.println();
    } // end method forEach

    /**
     * Checks if the appropriate URL, password, and username are valid;
     * designates the JDBC to the specified localhost port and verifies
     * the connectivity to the defined database table.
     */
    public static void connectDatabase() throws ClassNotFoundException
    {
        StringBuilder db_spec = new StringBuilder();
        db_spec.ensureCapacity(50);

        int insertionPoint = 51;
        int size = 0;
        String dbName = "Healthcare_Db";
        while (insertionPoint > db_spec.capacity())
        {
            db_spec.append("jdbc:mysql://localhost/practising");

            db_spec.insert(33, "root");

            size = db_spec.length();
            insertionPoint--;
        }

        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            coreConnectTask = new DatabaseConnection(db_spec.substring(0, 33), db_spec.substring(33, 37), "");

            displayResultSet();

            System.out.printf("The Patient Number is: %d%nThe Patient Name is: %s%n",
                    coreConnectTask.getPatients().get(0).getPatient_number(),
                    coreConnectTask.getPatients().get(0).getPatient_name());

            out.printf("%nConnection to '%s' database was successfully established%n",
                    dbName.subSequence(0, dbName.length()));

            db_spec.delete(0, size);
        }
        catch (Exception sqlException) {
            DatabaseConnection.database_logger(sqlException);
            out.printf("\nFailed to establish connection to database.\n%s%n", sqlException);
        }
    }


    private static void displayResultSet() {
        out.println("The query used for retrieving the Patient Number and Name is: " + SELECT_QUERY);
    }
    /**
     * Manages the content displayed based on the specified user
     * input value, must be in the range 1-2. If exceeds the
     * input range limit, the loop will terminate.
     */
    private static void contentManager()
    {
        main_consoleMenu();
        int userInput = input.nextInt();

        while (userInput != 3)
        {
            switch (userInput)
            {
                case 1:
                    sub_consoleMenu();
                    break;

                case 2:
                    new StatisticalTest().distributed_mean();
                    break;
            }
            main_consoleMenu();
            userInput = input.nextInt();
        }
    } // end method contentManager

    // Display the main-menu to the caller method
    private static void main_consoleMenu() {
//        out.printf("%nSelect an Option: %n%s%n%s%n%s%n",
//                "1. View Academic Data",
//                "2. Compute Mean",
//                "3. Exit");
//        out.println("? ");
    } // end method main_consoleMenu

    // Display the sub-menu to the caller method.
    private static void sub_consoleMenu() {
        out.printf("%nSelect an Option: %n%s%n%s%n%s%n%s%n%s%n%s%n",
                "1.1  View Students",
                "1.2  View Grades",
                "1.3  Insert Student",
                "1.4  Update Student",
                "1.5  Delete Student",
                "1.6  Return to Main Menu");
        out.println("? ");
        String request = input.next();

        switch (request) {
            case "1.1":
                students();
                break;

            case "1.2":
                grades();
                break;

            case "1.3":
                coreConnectTask.optionalQueries(1);
                break;

            case "1.4":
                coreConnectTask.optionalQueries(2);
                break;

            case "1.5":
                coreConnectTask.optionalQueries(3);
                break;

            case "1.6":
                contentManager();
                break;
        }
    } // end sub_consoleMenu

    /**
     * Calculates the distributed mean value for the {@code Grade} class
     * and returns the sum of the elements of list divided by the
     * number of elements currently in the list.
     */
    public void distributed_mean()
    {
        int expected = 59;
        DatabaseConnection coreTask;
        List<Integer> mean_sum = new ArrayList<>();

        try {
            coreTask = new DatabaseConnection(URL, USERNAME, PASSWORD);
            int bound = coreTask.getStatistics().size();
            IntStream.range(0, bound).forEach(index ->
            {
                mean_sum.add(index, coreTask.getStatistics()
                        .get(index)
                        .getMark());
            });

            int sumStatistics = mean_sum.stream().mapToInt(Integer::intValue).sum();
            int mean = sumStatistics / mean_sum.size();
        }
        catch (Exception sqlException) {
            sqlException.printStackTrace();
        }
    }
}

    // Method for continued learning with confusing output
//    private static int distributed_mean()
//    {
//        List< Integer > mean_sum = new ArrayList<>();
//        int bound = createConnection.getGrades().size();
//
//        IntStream.range( 0, grades().size() ).forEach( index ->
//        {
//            mean_sum.add(index, createConnection.getGrades()
//                    .get(index)
//                    .getMark());
//        });
//
//        int sumStatistics = mean_sum.stream().mapToInt(Integer::intValue).sum();
//        return sumStatistics >> 1;
//    }
